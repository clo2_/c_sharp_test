﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using GongSolutions.Wpf.DragDrop;

//ref https://qiita.com/shiroma77_/items/0b8c97307ee52c67d0d2

namespace WpfApp1
{
    class SampleWindowViewModel : IDropTarget
    {
        public ObservableCollection<string> Files { get; } = new ObservableCollection<string>();

        public SampleWindowViewModel()
        {
            Files = new ObservableCollection<string>();
        }

        public void DragOver(IDropInfo dropInfo)
        {
            var dragFileList = ((DataObject)dropInfo.Data).GetFileDropList().Cast<string>();
            dropInfo.Effects = dragFileList.Any(_ =>
            {
                return IsCsv(_);
            }) ? DragDropEffects.Copy : DragDropEffects.None;
        }

        public void Drop(IDropInfo dropInfo)
        {
            var dragFileList = ((DataObject)dropInfo.Data).GetFileDropList().Cast<string>();
            dropInfo.Effects = dragFileList.Any(_ =>
            {
                return IsCsv(_);
            }) ? DragDropEffects.Copy : DragDropEffects.None;

            foreach (var file in dragFileList)
            {
                if (IsCsv(file))
                {
                    Files.Add(file);
                }
            }
        }

        private bool IsCsv(string data)
        {
            var extension = Path.GetExtension(data);
            return extension != null && extension == ".csv";
        }
    }
}
