﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace WpfApp1
{
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// https://blog.okazuki.jp/entry/2014/08/17/142413
    /// 
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            // UIスレッドからの普通の呼び出しなのでOK
            var d = new DrivedObject();
            d.DoSomething();
        }

        private async void NGButton_Click(object sender, RoutedEventArgs e)
        {
            // UIスレッド以外からの呼び出しなので例外が出る
            var d = new DrivedObject();
            try
            {
                await Task.Run(() => d.DoSomething());
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
        }

        private async void DispatcherButton_Click(object sender, RoutedEventArgs e)
        {
            // UIスレッド以外だがDispatcher経由での呼び出しなのでOK
            var d = new DrivedObject();
            await Task.Run(async () =>
            {
                if (!d.CheckAccess())
                {
                    await d.Dispatcher.InvokeAsync(() => d.DoSomething()); // OK
                }
            });
        }


        public class DrivedObject : DispatcherObject
        {
            public void DoSomething()
            {
                // UIスレッドからのアクセスかチェックする
                this.VerifyAccess();
                Debug.WriteLine("DoSomething");
            }
        }





        private async void add01_Click(object sender, RoutedEventArgs e)
        {
            //           string text = await HeavyProcAsync();
            // Task.Runが禁止 らしいのでどうしたものか。。。Threadを使えということ？？
            string text = await Task.Run(() =>
            {
                return HeavyProc();
            });

            Debug.WriteLine("OK:" + text + "Sec");
            // this.label1.Text = "OK:" + text + "Sec";

        }

        /*
        public static async Task<string> HeavyTaskAsync()
        {
            ↓に乗っ取った書き方がまだわかっていない TODO
            https://www.kekyo.net/2016/12/06/6186
//            string aaa = await HeavyProc();
  //              return aaa;
        }
        */


        public static string HeavyProc()
        {
            var start = DateTime.Now;

            //何らかの重い処理
            System.Threading.Thread.Sleep(5000);

            var ts = DateTime.Now - start;
            return ts.TotalSeconds.ToString();

        }




    }




}
